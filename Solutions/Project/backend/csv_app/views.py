import pandas as pd
from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from .forms import CSVFileForm
from .models import CSVFile
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder, LabelEncoder
from sklearn.metrics import silhouette_score
import pickle
import os
import json

MODELS_DIR = 'models/'
ENCODERS_DIR = 'encoders/'

if not os.path.exists(MODELS_DIR):
    os.makedirs(MODELS_DIR)

if not os.path.exists(ENCODERS_DIR):
    os.makedirs(ENCODERS_DIR)

def make_prediction(request, pk, algorithm):
    model_path = os.path.join(MODELS_DIR, f'{algorithm}_model_{pk}.pkl')
    if not os.path.exists(model_path):
        return JsonResponse({'error': 'Model not found'}, status=404)

    with open(model_path, 'rb') as f:
        model = pickle.load(f)

    data = json.loads(request.body).get('data')
    if not data:
        return JsonResponse({'error': 'No data provided for prediction'}, status=400)

    df = pd.DataFrame(data)
    try:
        predictions = model.predict(df)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)

    if algorithm == 'classification':
        encoder_path = os.path.join(ENCODERS_DIR, f'label_encoder_{pk}.pkl')
        if os.path.exists(encoder_path):
            with open(encoder_path, 'rb') as f:
                label_encoder = pickle.load(f)
            predictions = label_encoder.inverse_transform(predictions)

    return JsonResponse({'predictions': predictions.tolist()})

def preprocess_data(df, columns):
    numeric_columns = df[columns].select_dtypes(include=['number']).columns.tolist()
    categorical_columns = df[columns].select_dtypes(exclude=['number']).columns.tolist()

    df[numeric_columns] = df[numeric_columns].fillna(df[numeric_columns].mean())
    df[categorical_columns] = df[categorical_columns].fillna('missing')

    processed_columns = numeric_columns.copy()

    if categorical_columns:
        encoder = OneHotEncoder(drop='first', sparse_output=False)
        encoded_cats = pd.DataFrame(encoder.fit_transform(df[categorical_columns]))
        encoded_cats.columns = encoder.get_feature_names_out(categorical_columns)
        encoded_cats.index = df.index 
        df = df.drop(categorical_columns, axis=1)
        df = pd.concat([df, encoded_cats], axis=1)
        processed_columns.extend(encoded_cats.columns.tolist())

    return df[processed_columns]

def train_model(request, pk):
    csv_file = get_object_or_404(CSVFile, pk=pk)
    df = pd.read_csv(csv_file.file.path)
    data = json.loads(request.body)
    columns = data.get('columns', [])
    target_column = data.get('target_column', None)
    algorithm = data.get('algorithm', '')
    n_clusters = data.get('n_clusters', 3)

    if not columns or not algorithm:
        return JsonResponse({'error': 'Columns and algorithm are required'}, status=400)

    if target_column in columns:
        columns.remove(target_column)

    X = df[columns]
    X = preprocess_data(df, columns)

    if target_column in X.columns:
        X = X.drop(columns=[target_column])

    if algorithm != 'clustering' and not target_column:
        return JsonResponse({'error': 'Target column is required for regression and classification'}, status=400)
    
    if algorithm != 'clustering':
        y = df[target_column]
        if y.dtype == 'object':
            label_encoder = LabelEncoder()
            y = label_encoder.fit_transform(y)
            encoder_path = os.path.join(ENCODERS_DIR, f'label_encoder_{pk}.pkl')
            with open(encoder_path, 'wb') as f:
                pickle.dump(label_encoder, f)
        
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        scaler = StandardScaler()
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

    else:
        scaler = StandardScaler()
        X = scaler.fit_transform(X)

    if algorithm == 'regression':
        model = LinearRegression()
        model.fit(X_train, y_train)
        score = model.score(X_test, y_test)

    elif algorithm == 'classification':
        model = RandomForestClassifier()
        model.fit(X_train, y_train)
        score = model.score(X_test, y_test)

    elif algorithm == 'clustering':
        model = KMeans(n_clusters=int(n_clusters))
        df['cluster'] = model.fit_predict(X)
        df.to_csv(csv_file.file.path, index=False)
        score = model.inertia_

    model_path = os.path.join(MODELS_DIR, f'{algorithm}_model_{pk}.pkl')
    with open(model_path, 'wb') as f:
        pickle.dump(model, f)

    return JsonResponse({'status': f'{algorithm.capitalize()} model trained successfully', 'score': score, 'preprocessing': 'Standard scaling and encoding applied to the selected columns.'})


def get_trained_model_details(request, pk, algorithm):
    model_path = os.path.join(MODELS_DIR, f'{algorithm}_model_{pk}.pkl')
    if not os.path.exists(model_path):
        return JsonResponse({'error': 'Model not found'}, status=404)

    with open(model_path, 'rb') as f:
        model = pickle.load(f)

    return JsonResponse({'status': f'{algorithm.capitalize()} model loaded successfully', 'algorithm': algorithm})

def upload_file(request):
    if request.method == 'POST':
        form = CSVFileForm(request.POST, request.FILES)
        if form.is_valid():
            csv_file = form.save()
            return JsonResponse({'id': csv_file.id})
    else:
        form = CSVFileForm()
    return render(request, 'csv_app/upload.html', {'form': form})

def file_shape(request, pk):
    csv_file = CSVFile.objects.get(pk=pk)
    df = pd.read_csv(csv_file.file.path)
    shape = df.shape
    return JsonResponse({'rows': shape[0], 'columns': shape[1]})

def file_columns(request, pk):
    csv_file = CSVFile.objects.get(pk=pk)
    df = pd.read_csv(csv_file.file.path)
    columns = {col: str(dtype) for col, dtype in df.dtypes.to_dict().items()}
    return JsonResponse(columns)

def file_rows(request, pk, direction, count):
    csv_file = CSVFile.objects.get(pk=pk)
    df = pd.read_csv(csv_file.file.path)
    count = int(count) 
    if direction == 'first':
        rows = df.head(count).fillna('').to_dict(orient='records')
    elif direction == 'last':
        rows = df.tail(count).fillna('').to_dict(orient='records')
    else:
        return JsonResponse({'error': 'Invalid direction'}, status=400)
    return JsonResponse(rows, safe=False)

def file_statistics(request, pk):
    csv_file = CSVFile.objects.get(pk=pk)
    df = pd.read_csv(csv_file.file.path)
    stats = df.describe().to_dict()
    stats = {k: {inner_k: str(v) for inner_k, v in inner_v.items()} for k, inner_v in stats.items()}
    return JsonResponse(stats)

def remove_empty_rows(request, pk):
    csv_file = CSVFile.objects.get(pk=pk)
    df = pd.read_csv(csv_file.file.path)
    df.dropna(inplace=True)
    df.to_csv(csv_file.file.path, index=False)
    return JsonResponse({'status': 'Rows with empty values removed'})
