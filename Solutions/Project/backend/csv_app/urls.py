from django.urls import path
from . import views

urlpatterns = [
    path('upload/', views.upload_file, name='upload_file'),
    path('shape/<int:pk>/', views.file_shape, name='file_shape'),
    path('columns/<int:pk>/', views.file_columns, name='file_columns'),
    path('rows/<int:pk>/<str:direction>/<int:count>/', views.file_rows, name='file_rows'),
    path('statistics/<int:pk>/', views.file_statistics, name='file_statistics'),
    path('remove_empty_rows/<int:pk>/', views.remove_empty_rows, name='remove_empty_rows'),
    path('train_model/<int:pk>/', views.train_model, name='train_model'),
    path('get_model_details/<int:pk>/<str:algorithm>/', views.get_trained_model_details, name='get_trained_model_details'),
    path('make_prediction/<int:pk>/<str:algorithm>/', views.make_prediction, name='make_prediction'),
]
