import React, { useState } from 'react';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import FileUpload from './components/FileUpload';
import FileDetails from './components/FileDetails';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

const theme = createTheme({
  palette: {
    mode: 'dark',
  },
});

function App() {
  const [fileId, setFileId] = useState(null);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container>
        <Box textAlign="center" my={4}>
          <Typography variant="h3" component="h1" gutterBottom>
            Programming Toolkit Project
          </Typography>
          <FileUpload setFileId={setFileId} />
          {fileId && <FileDetails fileId={fileId} />}
        </Box>
      </Container>
    </ThemeProvider>
  );
}

export default App;
