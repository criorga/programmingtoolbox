import React, { useState } from 'react';
import axios from 'axios';
import { Button, TextField, Typography, Alert, Select, MenuItem, FormControl, InputLabel, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';

function RowDisplay({ fileId }) {
  const [rows, setRows] = useState([]);
  const [error, setError] = useState(null);
  const [count, setCount] = useState(5);
  const [direction, setDirection] = useState('first');

  const fetchRows = async () => {
    try {
      const response = await axios.get(`http://localhost:8000/csv/rows/${fileId}/${direction}/${count}/`);
      setRows(Array.isArray(response.data) ? response.data : []);
      setError(null); 
    } catch (error) {
      setError(error.response?.data?.message || 'An error occurred while fetching rows.');
      setRows([]);
    }
  };

  return (
    <div>
      <Typography variant="h6">View Rows</Typography>
      <FormControl fullWidth margin="normal">
        <InputLabel>Direction</InputLabel>
        <Select value={direction} onChange={(e) => setDirection(e.target.value)}>
          <MenuItem value="first">First</MenuItem>
          <MenuItem value="last">Last</MenuItem>
        </Select>
      </FormControl>
      <TextField
        type="number"
        label="Number of Rows"
        value={count}
        onChange={(e) => setCount(e.target.value)}
        fullWidth
        margin="normal"
      />
      <Button variant="contained" color="primary" onClick={fetchRows}>
        Fetch Rows
      </Button>
      {error && <Alert severity="error">{error}</Alert>}
      {rows.length > 0 && (
        <TableContainer component={Paper} style={{ marginTop: '20px' }}>
          <Table>
            <TableHead>
              <TableRow>
                {Object.keys(rows[0]).map((key) => (
                  <TableCell key={key}>{key}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row, index) => (
                <TableRow key={index}>
                  {Object.values(row).map((value, idx) => (
                    <TableCell key={idx}>{value === '' ? '' : value}</TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </div>
  );
}

export default RowDisplay;
