import React, { useState } from 'react';
import axios from 'axios';
import Button from '@mui/material/Button';
import { TextField, Typography, Alert } from '@mui/material';

function FileUpload({ setFileId }) {
  const [file, setFile] = useState(null);
  const [error, setError] = useState(null);

  const handleFileChange = (e) => {
    setFile(e.target.files[0]);
    setError(null); 
  };

  const handleUpload = async () => {
    if (!file) {
      setError('Please select a file to upload.');
      return;
    }

    const formData = new FormData();
    formData.append('file', file);

    try {
      const response = await axios.post('http://localhost:8000/csv/upload/', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      setFileId(response.data.id);
      setError(null); 
    } catch (error) {
      setError(error.response?.data?.message || 'An error occurred during file upload.');
    }
  };

  return (
    <div>
      <TextField type="file" onChange={handleFileChange} />
      <Button variant="contained" color="primary" onClick={handleUpload}>
        Upload
      </Button>
      {error && <Alert severity="error">{error}</Alert>}
    </div>
  );
}

export default FileUpload;
