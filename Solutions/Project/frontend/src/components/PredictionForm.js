import React, { useState } from 'react';
import axios from 'axios';
import { Button, TextField, Typography, Grid, Box, CircularProgress } from '@mui/material';

function PredictionForm({ fileId, algorithm, columns, targetColumn }) {
  const [inputRows, setInputRows] = useState([{}]);
  const [predictions, setPredictions] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const handleInputChange = (index, column, value) => {
    const newRows = [...inputRows];
    newRows[index][column] = value;
    setInputRows(newRows);
  };

  const addRow = () => {
    setInputRows([...inputRows, {}]);
  };

  const preprocessRows = (rows) => {
    return rows;
  };

  const handleSubmit = async () => {
    setLoading(true);
    setError('');
    try {
      const preprocessedRows = preprocessRows(inputRows);
      const response = await axios.post(`http://localhost:8000/csv/make_prediction/${fileId}/${algorithm}/`, {
        data: preprocessedRows,
      });
      setPredictions(response.data.predictions);
    } catch (error) {
      setError(error.response?.data?.error || 'An error occurred while making the prediction.');
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <Typography variant="h6">Make Predictions</Typography>
      {inputRows.map((row, rowIndex) => (
        <Box key={rowIndex} mb={2}>
          <Grid container spacing={2}>
            {columns.map((column) => (
              <Grid item xs={12} sm={6} md={4} key={column}>
                <TextField
                  label={column}
                  value={row[column] || ''}
                  onChange={(e) => handleInputChange(rowIndex, column, e.target.value)}
                  fullWidth
                />
              </Grid>
            ))}
          </Grid>
        </Box>
      ))}
      <Button variant="contained" color="primary" onClick={addRow} disabled={loading}>
        Add Row
      </Button>
      <Box mt={2}>
        <Button variant="contained" color="primary" onClick={handleSubmit} disabled={loading}>
          {loading ? <CircularProgress size={24} /> : 'Submit'}
        </Button>
      </Box>
      {error && (
        <Box mt={2}>
          <Typography color="error">{error}</Typography>
        </Box>
      )}
      {predictions.length > 0 && (
        <Box mt={2}>
          <Typography variant="h6">Predictions</Typography>
          {predictions.map((prediction, index) => (
            <Typography key={index}>{`Prediction for row ${index + 1} (${targetColumn}): ${prediction}`}</Typography>
          ))}
        </Box>
      )}
    </div>
  );
}

export default PredictionForm;
