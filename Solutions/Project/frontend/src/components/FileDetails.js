import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Button, Alert, Typography } from '@mui/material';
import RowDisplay from './RowDisplay';
import RemoveEmptyRows from './RemoveEmptyRows';
import AITrainingModal from './AITrainingModal';
import PredictionForm from './PredictionForm';
import StatisticsDialog from './StatisticsDialog';

function FileDetails({ fileId }) {
  const [details, setDetails] = useState(null);
  const [error, setError] = useState(null);
  const [showColumns, setShowColumns] = useState(false);
  const [showAITraining, setShowAITraining] = useState(false);
  const [showStatistics, setShowStatistics] = useState(false);
  const [trainedModelDetails, setTrainedModelDetails] = useState(null);
  const [selectedAlgorithm, setSelectedAlgorithm] = useState('');
  const [selectedTrainingColumns, setSelectedTrainingColumns] = useState([]);
  const [targetColumn, setTargetColumn] = useState('');

  useEffect(() => {
    const fetchDetails = async () => {
      try {
        const shapeResponse = await axios.get(`http://localhost:8000/csv/shape/${fileId}/`);
        const columnsResponse = await axios.get(`http://localhost:8000/csv/columns/${fileId}/`);
        setDetails({ shape: shapeResponse.data, columns: columnsResponse.data });
        setError(null); 
      } catch (error) {
        setError(error.response?.data?.message || 'An error occurred while fetching file details.');
      }
    };

    if (fileId) {
      fetchDetails();
    }
  }, [fileId]);

  const toggleColumns = () => {
    setShowColumns(!showColumns);
  };

  const handleRowsRemoved = () => {
    const fetchDetails = async () => {
      try {
        const shapeResponse = await axios.get(`http://localhost:8000/csv/shape/${fileId}/`);
        setDetails({ ...details, shape: shapeResponse.data });
        setError(null);
      } catch (error) {
        setError(error.response?.data?.message || 'An error occurred while fetching file details.');
      }
    };

    fetchDetails();
  };

  const handleModelTrained = async (algorithm, selectedColumns) => {
    setSelectedAlgorithm(algorithm);
    setSelectedTrainingColumns(selectedColumns);
    setTargetColumn(selectedColumns.find(col => col === targetColumn));
    try {
      const response = await axios.get(`http://localhost:8000/csv/get_model_details/${fileId}/${algorithm}`);
      setTrainedModelDetails(response.data);
    } catch (error) {
      setTrainedModelDetails('An error occurred while fetching model details.');
    }
  };

  if (!details) {
    return error ? <Alert severity="error">{error}</Alert> : null;
  }

  return (
    <div>
      <Typography variant="h6">File Details</Typography>
      <Typography>Rows: {details.shape.rows}</Typography>
      <Typography>Columns: {details.shape.columns}</Typography>
      <Button variant="contained" color="primary" onClick={toggleColumns}>
        {showColumns ? 'Hide' : 'Show'} Column Details
      </Button>
      {showColumns && (
        <div>
          <Typography variant="h6">Column Details</Typography>
          <ul>
            {Object.entries(details.columns).map(([name, dtype]) => (
              <li key={name}>{name}: {dtype}</li>
            ))}
          </ul>
        </div>
      )}
      <RowDisplay fileId={fileId} />
      <RemoveEmptyRows fileId={fileId} onRowsRemoved={handleRowsRemoved} />
      <Button variant="contained" color="primary" onClick={() => setShowAITraining(true)}>
        Train AI Model
      </Button>
      <Button variant="contained" color="secondary" onClick={() => setShowStatistics(true)}>
        Show Statistics
      </Button>
      <AITrainingModal
        open={showAITraining}
        onClose={() => setShowAITraining(false)}
        fileId={fileId}
        onModelTrained={handleModelTrained}
      />
      <StatisticsDialog
        open={showStatistics}
        onClose={() => setShowStatistics(false)}
        fileId={fileId}
      />
      {trainedModelDetails && (
        <div>
          <Typography variant="h6">Trained Model Details</Typography>
          <Typography>{trainedModelDetails.status}</Typography>
          <PredictionForm
            fileId={fileId}
            algorithm={selectedAlgorithm}
            columns={selectedTrainingColumns}
            targetColumn={targetColumn}
          />
        </div>
      )}
    </div>
  );
}

export default FileDetails;
