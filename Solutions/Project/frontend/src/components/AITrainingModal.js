import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
  Modal, Backdrop, Fade, Button, FormControl, InputLabel, Select, MenuItem, Checkbox, ListItemText,
  CircularProgress, Typography, TextField, Box
} from '@mui/material';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';

function AITrainingModal({ open, onClose, fileId, onModelTrained }) {
  const [columns, setColumns] = useState([]);
  const [selectedColumns, setSelectedColumns] = useState([]);
  const [targetColumn, setTargetColumn] = useState('');
  const [algorithm, setAlgorithm] = useState('');
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState('');
  const [score, setScore] = useState('');
  const [preprocessing, setPreprocessing] = useState('');
  const [nClusters, setNClusters] = useState(3);
  const [columnTypes, setColumnTypes] = useState({});

  useEffect(() => {
    const fetchColumns = async () => {
      const response = await axios.get(`http://localhost:8000/csv/columns/${fileId}/`);
      setColumns(Object.keys(response.data));
      setColumnTypes(response.data);
    };

    fetchColumns();
  }, [fileId]);

  const handleAlgorithmChange = (e) => {
    setAlgorithm(e.target.value);
    setTargetColumn('');
  };

  const handleTargetColumnChange = (e) => {
    setTargetColumn(e.target.value);
    setSelectedColumns(selectedColumns.filter(col => col !== e.target.value));
  };

  const handleTrainModel = async () => {
    setLoading(true);
    try {
      const response = await axios.post(`http://localhost:8000/csv/train_model/${fileId}/`, {
        columns: selectedColumns,
        target_column: targetColumn,
        algorithm,
        n_clusters: algorithm === 'clustering' ? nClusters : undefined,
      });
      setStatus(response.data.status);
      setScore(response.data.score);
      setPreprocessing(response.data.preprocessing);
      setTimeout(() => {
        setLoading(false);
        setTimeout(() => {
          onClose();
          onModelTrained(algorithm, selectedColumns); 
        }, 2000);
      }, 2000);
    } catch (error) {
      setStatus('An error occurred while training the model.');
      setLoading(false);
    }
  };

  const isColumnValidForAlgorithm = (column) => {
    const dtype = columnTypes[column];
    if (algorithm === 'classification') {
      return ['object', 'bool', 'category'].includes(dtype); 
    }
    if (algorithm === 'regression') {
      return ['int64', 'float64'].includes(dtype);
    }
    return true;
  };

  return (
    <Modal
      open={open}
      onClose={onClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <div style={{ padding: '20px', backgroundColor: '#fff', margin: 'auto', width: '50%' }}>
          <Typography variant="h6">AI Training</Typography>
          <FormControl fullWidth margin="normal">
            <InputLabel>Algorithm</InputLabel>
            <Select value={algorithm} onChange={handleAlgorithmChange}>
              <MenuItem value="regression">Regression</MenuItem>
              <MenuItem value="classification">Classification</MenuItem>
              <MenuItem value="clustering">Clustering</MenuItem>
            </Select>
          </FormControl>
          <FormControl fullWidth margin="normal">
            <InputLabel>Columns</InputLabel>
            <Select
              multiple
              value={selectedColumns}
              onChange={(e) => setSelectedColumns(e.target.value)}
              renderValue={(selected) => selected.join(', ')}
            >
              {columns.filter(col => col !== targetColumn).map((column) => (
                <MenuItem key={column} value={column}>
                  <Checkbox checked={selectedColumns.indexOf(column) > -1} />
                  <ListItemText primary={column} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          {algorithm !== 'clustering' && (
            <FormControl fullWidth margin="normal">
              <InputLabel>Target Column</InputLabel>
              <Select value={targetColumn} onChange={handleTargetColumnChange}>
                {columns.filter(isColumnValidForAlgorithm).map((column) => (
                  <MenuItem key={column} value={column}>
                    {column}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          )}
          {algorithm === 'clustering' && (
            <TextField
              type="number"
              label="Number of Clusters"
              value={nClusters}
              onChange={(e) => setNClusters(e.target.value)}
              fullWidth
              margin="normal"
            />
          )}
          <Button variant="contained" color="primary" onClick={handleTrainModel} disabled={loading}>
            {loading ? <CircularProgress size={24} /> : 'Train Model'}
          </Button>
          {status && (
            <Box display="flex" alignItems="center" marginTop="16px">
              <CheckCircleOutlineIcon color="success" />
              <Typography marginLeft="8px">{status}</Typography>
            </Box>
          )}
          {score && <Typography>Score: {score}</Typography>}
          {preprocessing && <Typography>{preprocessing}</Typography>}
        </div>
      </Fade>
    </Modal>
  );
}

export default AITrainingModal;
