import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Dialog, DialogActions, DialogContent, DialogTitle, Button, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';

function StatisticsDialog({ fileId, open, onClose }) {
  const [stats, setStats] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    if (open && fileId) {
      const fetchStats = async () => {
        try {
          const response = await axios.get(`http://localhost:8000/csv/statistics/${fileId}/`);
          setStats(response.data);
          setError(null); 
        } catch (error) {
          setError(error.response?.data?.message || 'An error occurred while fetching statistics.');
        }
      };

      fetchStats();
    }
  }, [open, fileId]);

  return (
    <Dialog open={open} onClose={onClose} maxWidth="md" fullWidth>
      <DialogTitle>Dataset Statistics</DialogTitle>
      <DialogContent>
        {error && <Typography color="error">{error}</Typography>}
        {stats && (
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Statistic</TableCell>
                  {Object.keys(stats).map((key) => (
                    <TableCell key={key}>{key}</TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.keys(stats[Object.keys(stats)[0]]).map((stat) => (
                  <TableRow key={stat}>
                    <TableCell>{stat}</TableCell>
                    {Object.keys(stats).map((key) => (
                      <TableCell key={key}>{stats[key][stat]}</TableCell>
                    ))}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">Close</Button>
      </DialogActions>
    </Dialog>
  );
}

export default StatisticsDialog;
