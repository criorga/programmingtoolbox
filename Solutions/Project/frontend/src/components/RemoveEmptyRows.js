import React, { useState } from 'react';
import axios from 'axios';
import { Button, Alert } from '@mui/material';

function RemoveEmptyRows({ fileId, onRowsRemoved }) {
  const [status, setStatus] = useState(null);
  const [error, setError] = useState(null);

  const handleRemoveEmptyRows = async () => {
    try {
      const response = await axios.get(`http://localhost:8000/csv/remove_empty_rows/${fileId}/`);
      setStatus(response.data.status);
      setError(null);
      if (onRowsRemoved) {
        onRowsRemoved();
      }
    } catch (error) {
      setError('An error occurred while removing empty rows.');
      setStatus(null);
    }
  };

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleRemoveEmptyRows}>
        Remove Rows with Empty Values
      </Button>
      {status && <Alert severity="success">{status}</Alert>}
      {error && <Alert severity="error">{error}</Alert>}
    </div>
  );
}

export default RemoveEmptyRows;
