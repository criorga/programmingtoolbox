import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Professional = () => {
  const [workExperience, setWorkExperience] = useState([]);
  const [education, setEducation] = useState([]);

  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/workexperience/')
      .then(response => {
        setWorkExperience(response.data);
      })
      .catch(error => {
        console.error("There was an error fetching the work experience data!", error);
      });

    axios.get('http://127.0.0.1:8000/api/personaldata/')
      .then(response => {
        setEducation(response.data[0].education.split(','));
      })
      .catch(error => {
        console.error("There was an error fetching the education data!", error);
      });
  }, []);

  return (
    <div className="section professional">
      <h2>Professional Experience</h2>
      <ul>
        {workExperience.map((job, index) => (
          <li key={index}>
            {job.role} at {job.company} ({job.duration})
          </li>
        ))}
      </ul>
      <h3>Education</h3>
      <ul>
        {education.map((edu, index) => (
          <li key={index}>{edu}</li>
        ))}
      </ul>
    </div>
  );
};

export default Professional;
