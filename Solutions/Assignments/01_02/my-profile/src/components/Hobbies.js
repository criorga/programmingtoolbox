import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Hobbies = () => {
  const [hobbies, setHobbies] = useState([]);

  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/hobbies/')
      .then(response => {
        setHobbies(response.data);
      })
      .catch(error => {
        console.error("There was an error fetching the hobbies data!", error);
      });
  }, []);

  return (
    <div className="section hobbies">
      <h2>Hobbies</h2>
      <ul>
        {hobbies.map((hobby, index) => (
          <li key={index}>{hobby.name}</li>
        ))}
      </ul>
    </div>
  );
};

export default Hobbies;
