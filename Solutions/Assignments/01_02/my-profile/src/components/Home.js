import React, { useEffect, useState } from 'react';
import axios from 'axios';

const Home = () => {
  const [personalInfo, setPersonalInfo] = useState(null);

  useEffect(() => {
    axios.get('http://127.0.0.1:8000/api/personaldata/')
      .then(response => {
        setPersonalInfo(response.data[0]);
      })
      .catch(error => {
        console.error("There was an error fetching the personal data!", error);
      });
  }, []);

  if (!personalInfo) return <div>Loading...</div>;

  return (
    <div className="section home">
      <h1>{personalInfo.name}</h1>
      <p>Date of Birth: {personalInfo.date_of_birth}</p>
      <p>Education: {personalInfo.education}</p>
      <p>Places Lived:</p>
      <ul>
        {personalInfo.places_lived.split(',').map((place, index) => (
          <li key={index}>{place}</li>
        ))}
      </ul>
    </div>
  );
};

export default Home;
