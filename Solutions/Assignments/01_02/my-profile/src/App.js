import React from 'react';
import Home from './components/Home';
import Professional from './components/Professional';
import Hobbies from './components/Hobbies';
import './styles.css';

const App = () => {
  return (
    <div className="app">
      <nav>
        <ul>
          <li><a href="#home">Home</a></li>
          <li><a href="#professional">Professional</a></li>
          <li><a href="#hobbies">Hobbies</a></li>
        </ul>
      </nav>
      <div id="home">
        <Home />
      </div>
      <div id="professional">
        <Professional />
      </div>
      <div id="hobbies">
        <Hobbies />
      </div>
    </div>
  );
};

export default App;
