const profileData = {
    personalInfo: {
        name: "Iorga Cristian",
        dateOfBirth: "22-12-2000",
        education: "Bachelor's in Computer Science",
        placesLived: ["Cluj-Napoca, Romania"]
    },
    professionalInfo: {
        jobs: [
            {
                company: "Devnest",
                role: "Junior Blockchain Developer",
                duration: "2022-2023"
            },
            {
                company: "Junior Data Consultant",
                role: "Cavell Group",
                duration: "2023-present"
            }
        ],
        education: ["Bachelor's in Software Engineering", "Studying for Master's in Data Science"]
    },
    hobbies: ["Videogames", "Fantasy worlds", "Music", "Data Science"]
};

export default profileData;
