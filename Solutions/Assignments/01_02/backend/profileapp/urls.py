from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import PersonalDataViewSet, WorkExperienceViewSet, HobbyViewSet

router = DefaultRouter()
router.register(r'personaldata', PersonalDataViewSet)
router.register(r'workexperience', WorkExperienceViewSet)
router.register(r'hobbies', HobbyViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
