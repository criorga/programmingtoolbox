from django.db import models

class PersonalData(models.Model):
    name = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    education = models.CharField(max_length=255)
    places_lived = models.TextField()

    def __str__(self):
        return self.name

class WorkExperience(models.Model):
    company = models.CharField(max_length=100)
    role = models.CharField(max_length=100)
    duration = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.role} at {self.company}"

class Hobby(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
